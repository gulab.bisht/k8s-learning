## Kubernetes Crash Course: Learn the Basics and Build a Microservice Application
* https://www.youtube.com/watch?v=XuSQU5Grv1g

## Example Voting App Kubernetes

This is based on the original [example-voting-app](https://github.com/dockersamples/example-voting-app) repository from the [docker-examples](https://github.com/dockersamples) GitHub page

and modified it to work on the Kubernetes cluster.


